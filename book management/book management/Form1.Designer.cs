﻿namespace book_management {
    partial class FormBookManagement {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.gBoxNewBook = new System.Windows.Forms.GroupBox();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblPublisher = new System.Windows.Forms.Label();
            this.lblEdition = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.lblAuthor = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.cboxPublisher = new System.Windows.Forms.ComboBox();
            this.txtEdition = new System.Windows.Forms.TextBox();
            this.txtYear = new System.Windows.Forms.TextBox();
            this.txtAuthor = new System.Windows.Forms.TextBox();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.lstBooks = new System.Windows.Forms.ListBox();
            this.gBoxListEdit = new System.Windows.Forms.GroupBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.gBoxNewBook.SuspendLayout();
            this.gBoxListEdit.SuspendLayout();
            this.SuspendLayout();
            // 
            // gBoxNewBook
            // 
            this.gBoxNewBook.Controls.Add(this.btnUpdate);
            this.gBoxNewBook.Controls.Add(this.lblDescription);
            this.gBoxNewBook.Controls.Add(this.txtDescription);
            this.gBoxNewBook.Controls.Add(this.btnSave);
            this.gBoxNewBook.Controls.Add(this.lblPublisher);
            this.gBoxNewBook.Controls.Add(this.lblEdition);
            this.gBoxNewBook.Controls.Add(this.lblYear);
            this.gBoxNewBook.Controls.Add(this.lblAuthor);
            this.gBoxNewBook.Controls.Add(this.lblTitle);
            this.gBoxNewBook.Controls.Add(this.cboxPublisher);
            this.gBoxNewBook.Controls.Add(this.txtEdition);
            this.gBoxNewBook.Controls.Add(this.txtYear);
            this.gBoxNewBook.Controls.Add(this.txtAuthor);
            this.gBoxNewBook.Controls.Add(this.txtTitle);
            this.gBoxNewBook.Location = new System.Drawing.Point(13, 13);
            this.gBoxNewBook.Name = "gBoxNewBook";
            this.gBoxNewBook.Size = new System.Drawing.Size(943, 134);
            this.gBoxNewBook.TabIndex = 0;
            this.gBoxNewBook.TabStop = false;
            this.gBoxNewBook.Text = "Nová kniha";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(3, 61);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(33, 13);
            this.lblDescription.TabIndex = 7;
            this.lblDescription.Text = "Popis";
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(6, 77);
            this.txtDescription.Multiline = true;
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(843, 51);
            this.txtDescription.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(859, 28);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Uložit";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblPublisher
            // 
            this.lblPublisher.AutoSize = true;
            this.lblPublisher.Location = new System.Drawing.Point(728, 14);
            this.lblPublisher.Name = "lblPublisher";
            this.lblPublisher.Size = new System.Drawing.Size(54, 13);
            this.lblPublisher.TabIndex = 2;
            this.lblPublisher.Text = "Vydavatel";
            // 
            // lblEdition
            // 
            this.lblEdition.AutoSize = true;
            this.lblEdition.Location = new System.Drawing.Point(621, 16);
            this.lblEdition.Name = "lblEdition";
            this.lblEdition.Size = new System.Drawing.Size(34, 13);
            this.lblEdition.TabIndex = 2;
            this.lblEdition.Text = "Edice";
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(515, 16);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(27, 13);
            this.lblYear.TabIndex = 2;
            this.lblYear.Text = "Rok";
            // 
            // lblAuthor
            // 
            this.lblAuthor.AutoSize = true;
            this.lblAuthor.Location = new System.Drawing.Point(309, 16);
            this.lblAuthor.Name = "lblAuthor";
            this.lblAuthor.Size = new System.Drawing.Size(32, 13);
            this.lblAuthor.TabIndex = 2;
            this.lblAuthor.Text = "Autor";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(6, 16);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(38, 13);
            this.lblTitle.TabIndex = 2;
            this.lblTitle.Text = "Název";
            // 
            // cboxPublisher
            // 
            this.cboxPublisher.FormattingEnabled = true;
            this.cboxPublisher.Location = new System.Drawing.Point(731, 30);
            this.cboxPublisher.Name = "cboxPublisher";
            this.cboxPublisher.Size = new System.Drawing.Size(121, 21);
            this.cboxPublisher.TabIndex = 4;
            // 
            // txtEdition
            // 
            this.txtEdition.Location = new System.Drawing.Point(624, 32);
            this.txtEdition.Name = "txtEdition";
            this.txtEdition.Size = new System.Drawing.Size(100, 20);
            this.txtEdition.TabIndex = 3;
            // 
            // txtYear
            // 
            this.txtYear.Location = new System.Drawing.Point(518, 32);
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(100, 20);
            this.txtYear.TabIndex = 2;
            // 
            // txtAuthor
            // 
            this.txtAuthor.Location = new System.Drawing.Point(312, 32);
            this.txtAuthor.Name = "txtAuthor";
            this.txtAuthor.Size = new System.Drawing.Size(200, 20);
            this.txtAuthor.TabIndex = 1;
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(6, 32);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(300, 20);
            this.txtTitle.TabIndex = 0;
            // 
            // lstBooks
            // 
            this.lstBooks.FormattingEnabled = true;
            this.lstBooks.Location = new System.Drawing.Point(6, 19);
            this.lstBooks.Name = "lstBooks";
            this.lstBooks.Size = new System.Drawing.Size(843, 264);
            this.lstBooks.TabIndex = 8;
            this.lstBooks.SelectedIndexChanged += new System.EventHandler(this.lstBooks_SelectedIndexChanged);
            // 
            // gBoxListEdit
            // 
            this.gBoxListEdit.Controls.Add(this.btnDelete);
            this.gBoxListEdit.Controls.Add(this.lstBooks);
            this.gBoxListEdit.Location = new System.Drawing.Point(13, 153);
            this.gBoxListEdit.Name = "gBoxListEdit";
            this.gBoxListEdit.Size = new System.Drawing.Size(943, 296);
            this.gBoxListEdit.TabIndex = 2;
            this.gBoxListEdit.TabStop = false;
            this.gBoxListEdit.Text = "Knihy";
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(859, 20);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 9;
            this.btnDelete.Text = "Odstranit";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(859, 77);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 7;
            this.btnUpdate.Text = "Aktualizuj";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // FormBookManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 461);
            this.Controls.Add(this.gBoxListEdit);
            this.Controls.Add(this.gBoxNewBook);
            this.Name = "FormBookManagement";
            this.Text = "Správa knih";
            this.Load += new System.EventHandler(this.FormBookManagement_Load);
            this.gBoxNewBook.ResumeLayout(false);
            this.gBoxNewBook.PerformLayout();
            this.gBoxListEdit.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gBoxNewBook;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblPublisher;
        private System.Windows.Forms.Label lblEdition;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Label lblAuthor;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.ComboBox cboxPublisher;
        private System.Windows.Forms.TextBox txtEdition;
        private System.Windows.Forms.TextBox txtYear;
        private System.Windows.Forms.TextBox txtAuthor;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.ListBox lstBooks;
        private System.Windows.Forms.GroupBox gBoxListEdit;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Button btnUpdate;
    }
}

