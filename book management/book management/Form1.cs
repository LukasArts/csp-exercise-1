﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace book_management {
    public partial class FormBookManagement : Form {
        public FormBookManagement() {
            InitializeComponent();
        }
        BindingList<Book> books = new BindingList<Book>();
        
        private void FormBookManagement_Load(object sender, EventArgs e) {

            List<Publisher> publishers = new List<Publisher>() {
                new Publisher("Albatros", "info@albatros.cz"),
                new Publisher("Odeon", "knihy@odeon.com"),
                new Publisher("Addison-Wesley", "info@pearson.de"),
                new Publisher("MIT Press", "books@mitpress.mit.edu")
            };

            lstBooks.DataSource = books;

            cboxPublisher.DataSource = publishers;
        }

        private List<string> validateInput() {
            List<string> errors = new List<string>();
            int number;

            if (txtTitle.Text.Length > 20) errors.Add("• Název může být nejvíce 20 znaků dlouhý.");
            if (txtAuthor.Text.Length > 20) errors.Add("• Jméno autora může být nejvýše 20 znaků dlouhé");
            if (!Int32.TryParse(txtYear.Text, out number ) || txtYear.Text.Length != 4) errors.Add("• Zadaný rok je neplatný.");
            if (!Int32.TryParse(txtEdition.Text, out number)) errors.Add("• Číslo edice je neplatné.");
            if (txtDescription.Text.Length > 100) errors.Add("• Popis může mít nejvíce 100 znaků.");

            return errors;
        }

        private void btnSave_Click(object sender, EventArgs e) {
            List<string> errors = validateInput();
            if (errors.Count != 0)
                MessageBox.Show(string.Join("\n", errors.ToArray()),"Chybný vstup", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else {
                Book newBook = new Book(txtTitle.Text, txtAuthor.Text, Int32.Parse(txtYear.Text), Int32.Parse(txtEdition.Text), DateTime.Now, (Publisher)cboxPublisher.SelectedItem, txtDescription.Text);
                bool unique = true;
                foreach( Book book in lstBooks.Items ) {
                    if (newBook.Equals(book)) unique = false;
                }
                if (unique)
                    books.Add(newBook);
                else
                    MessageBox.Show("Kniha již v seznamu je.", "Chybný vstup", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e) {
            if (lstBooks.SelectedItem != null) {
                books.RemoveAt(lstBooks.SelectedIndex);
            }
        }

        private void lstBooks_SelectedIndexChanged(object sender, EventArgs e) {
            if (lstBooks.SelectedItem != null) {
                txtTitle.Text = ((Book)lstBooks.SelectedItem)._title;
                txtAuthor.Text = ((Book)lstBooks.SelectedItem)._author;
                txtYear.Text = ((Book)lstBooks.SelectedItem)._year.ToString();
                txtEdition.Text = ((Book)lstBooks.SelectedItem)._edition.ToString();
                cboxPublisher.Text = ((Book)lstBooks.SelectedItem)._publisher.ToString();
                txtDescription.Text = ((Book)lstBooks.SelectedItem)._description;
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e) {
            var book = lstBooks.SelectedItem;
            if ( book != null ) {
                List<string> errors = validateInput();
                if (errors.Count != 0)
                    MessageBox.Show(string.Join("\n", errors.ToArray()), "Chybný vstup", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else {
                    (book as Book)._title = txtTitle.Text;
                    (book as Book)._author = txtAuthor.Text;
                    (book as Book)._year = Int32.Parse(txtYear.Text);
                    (book as Book)._edition = Int32.Parse(txtEdition.Text);
                    (book as Book)._description = txtDescription.Text;
                }
            }
            // hack to force redraw after update
            var list = lstBooks.DataSource;
            lstBooks.DataSource = null;
            lstBooks.DataSource = list;
        }
    }
}
