﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace book_management {
    static class Program {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormBookManagement());
        }
    }

    public class Book
    {
        public string _title;
        public string _author;
        public int _year;
        public int _edition;
        public readonly DateTime _creationDate;
        public Publisher _publisher;
        public string _description;

        public Book(string title,
            string author,
            int year,
            int edition,
            DateTime creationDate,
            Publisher publisher,
            string description)
        {
            _title = title;
            _author = author;
            _year = year;
            _edition = edition;
            _creationDate = creationDate;
            _publisher = publisher;
            _description = description;
        }

        public override bool Equals(object other) {
            if (!(other is Book)) return false;
            return base.Equals(other);
        }

        public bool Equals(Book other) =>
            _title == other._title && _author == other._author && _year == other._year &&
                _edition == other._edition && _publisher == other._publisher && _description == other._description;

        public override int GetHashCode() {
            return 31 * base.GetHashCode();
        }
        public static bool operator ==(Book lhs, Book rhs) => lhs.Equals(rhs);

        public static bool operator !=(Book lhs, Book rhs) => !lhs.Equals(rhs);

        public override string ToString() {
            return _title + " (" + _year + ", " + _edition + ". Edition)";
        }
    }

    public struct Publisher
    {
        private readonly string _name;
        private readonly string _email;

        public Publisher(string name, string email)
        {
            _name = name;
            _email = email;
        }

        public override bool Equals(object other) {
            if (!(other is Publisher)) return false;
            return base.Equals(other);
        }

        public bool Equals(Publisher other) =>
            _name == other._name && _email == other._email;

        public override int GetHashCode() {
            return 31 * base.GetHashCode();
        }

        public static bool operator == (Publisher lhs, Publisher rhs) => lhs.Equals(rhs);

        public static bool operator != (Publisher lhs, Publisher rhs) => !lhs.Equals(rhs);

        public override string ToString() {
            return _name;
        }
    }
}
